import 'reset-css';
import './css/style.css'

import React from 'react';
import {
  HashRouter,
  Route,
  Link
} from 'react-router-dom';
import ReactDOM from 'react-dom';
import Header from './components/header.jsx';
import Menu from './components/menu.jsx';
import Recents from './components/recents.jsx';
import Settings from './components/settings.jsx';
import Subscribed from './components/subscribed.jsx'
import MangaList from './components/mangalist.jsx';
import Manga from './components/manga.jsx';
import Chapter from './components/chapter.jsx';
import injectTapEventPlugin from 'react-tap-event-plugin';

import { Provider } from 'react-redux';
import store from './store'
injectTapEventPlugin();
ReactDOM.render((
		<Provider store={store}>
			<HashRouter>
					<div>
						<Header></Header>
            <Menu></Menu>
						<Route path="/" exact component={MangaList}/>
            <Route path={'/recents'} exact component={Recents}/>
            <Route path={'/subscribed'} exact component={Subscribed}/>
            <Route path={'/settings'} exact component={Settings}/>
            <Route path={'/m/:manga'} exact component={Manga}/>
            <Route path={'/m/:manga/:chapter'} component={Chapter}/>
					</div>
			</HashRouter>
		</Provider>),
	document.getElementById('root')
);
