import React from 'react';
import Router from 'react-router-dom';
import Route from 'react-router-dom';
import ReactDOM from 'react-dom';
import AppContainer from './components/app_container.jsx' 

ReactDOM.render(
	<Router>
		<Route path="/" component={AppContainer}/>
	</Router>,
	document.getElementById('root')
);