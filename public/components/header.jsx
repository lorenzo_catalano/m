import React from 'react';
import { connect } from 'react-redux';
import store from '../store';
import TelegramLoginButton from './telegram'

let el = function(e){
  document.removeEventListener('click',el);
  document.querySelector('#menu').className='';
}
class Header extends React.Component {
  handleTelegramResponse(){

  }
  componentDidMount(){
  }

  togglemenu(){
    if(document.querySelector('#menu').className=='open'){
      el()
    }else{
      document.querySelector('#menu').className='open'
      document.addEventListener('click',el)
    }
  }

  subscribe(){
    fetch('/s', {
      credentials: 'same-origin',
      method: 'POST',
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
      body: JSON.stringify({
        path:this.props.currentMangaPath,
        name:this.props.currentMangaName
      })
    }).then(()=>{
      store.dispatch({type:'SUBSCRIBE'});
    });
  }
  unsubscribe(){
    fetch('/s', {
      credentials: 'same-origin',
      method: 'DELETE',
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
      body: JSON.stringify({
        path:this.props.currentMangaPath,
        name:this.props.currentMangaName
      })
    }).then(()=>{
      store.dispatch({type:'UNSUBSCRIBE'});
    });
  }

  render(){
    let subbutton=null;
    let telegramlogin=null;

    if(this.props.logged && this.props.currentMangaPath){
      if(this.props.subscribed){
        subbutton=<div className="action-button list-item" onClick={this.unsubscribe.bind(this)}><i className="fa fa-bell-o" aria-hidden="true"></i></div>
      }else{
        subbutton=<div className="action-button list-item" onClick={this.subscribe.bind(this)}><i className="fa fa-bell-slash-o" aria-hidden="true"></i></div>
      }
    }

    if(this.props.logged){
      telegramlogin=<div></div>
    }else{
      telegramlogin=<TelegramLoginButton dataOnauth={this.handleTelegramResponse} botName="lorenz_notification_bot" />
    }

    return (
      <header>
        <span className="menu-button" onClick={this.togglemenu}>
          <i className="fa fa-bars" aria-hidden="true"></i>
        </span>
        <span className="title">{this.props.title}</span>
        <div>
        {telegramlogin}
        </div>
        {subbutton}

      </header>
  )
  }
}

const mapStateToProps = function(store) {
  return {
    subscribed: store.subscribed,
    title: store.title,
    logged: store.logged,
    currentMangaPath:store.currentMangaPath,
    currentMangaName:store.currentMangaName
  };
}

export default  connect(mapStateToProps)(Header);
