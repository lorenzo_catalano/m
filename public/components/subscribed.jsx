import React from 'react';
import MangaListItem from './manga/list-item';
import store from '../store';
import { connect } from 'react-redux';
import Loader from './loader';

class Subscribed extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        subscriptions : []
      };
    }

  componentDidMount() {
    store.dispatch({type:'SUBS_LOADING'});
    fetch('/s', {credentials: 'same-origin'})
    .then(response=>response.json())
    .then(json=>{
      store.dispatch({type:'SUBS_LOADED',subscriptions:json.subscriptions || []});
    })
  }

  render() {
    if(this.props.loading){
      return (<Loader/>)
    }else if(this.props.subscriptions){
      return (
            <ul>
                {this.props.subscriptions.map(row=><li key={row.name+row.url} ><MangaListItem manga={row} sub={true} /></li>)}
            </ul>
      );
    }else{
      return <ul></ul>
    }

  }
}

const mapStateToProps = function(store) {
  return {
    subscriptions:store.subscriptions,
    loading: store.loading
  };
}

export default connect(mapStateToProps)(Subscribed);
