import React from 'react';
import {Link} from 'react-router-dom';
import ChapterListItem from './chapter/list-item';
import store from '../store';
import { connect } from 'react-redux';
import Loader from './loader';

class Manga extends React.Component {

    componentDidMount() {
      //if(this.props.history.action!=='POP'){
        store.dispatch({type:'MANGA_LOADING'});
          fetch('m/'+this.props.match.params.manga, {credentials: 'same-origin'})
          .then(response=>response.json())
          .then(json=>{
              store.dispatch({type:'MANGA_LOADED',manga:json});
          })
      //}

    }

    render() {

      if(this.props.loading){
        return (<Loader/>)
      }else{
        return (
              <div>
                  {this.props.chapters.map(function(chapter) {
                    return (<ChapterListItem  key={chapter.path} chapter={chapter}/>)
                  })}
              </div>
        );
      }
    }
}

const mapStateToProps = function(store) {
  return {
    subscribed:store.subscribed,
    chapters: store.currentManga?store.currentManga.chapters:[],
    loading: store.loading
  };
}

export default connect(mapStateToProps)(Manga);
