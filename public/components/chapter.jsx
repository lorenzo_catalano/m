import React from 'react';
import {Link} from 'react-router-dom';
import store from '../store';
import { connect } from 'react-redux';
import Loader from './loader';
import {Preload} from 'react-preload';
var loadingIndicator = (<div className="imageLoader">Loading image...</div>)

let ticking = false;
let lastScrollY = 0;
let showfooter = false;
class Chapter extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      showfooter: false,
      listener : this.handleScroll.bind(this)
    }
  }

    componentWillUnmount() {
      window.removeEventListener('scroll', this.state.listener);
    }

    handleScroll(event){
      lastScrollY = window.scrollY;
      if (!ticking) {
        window.requestAnimationFrame(() => {
          if(lastScrollY + document.body.clientHeight > document.body.scrollHeight-60){
            this.setState({
              showfooter: true
            });
          }else{
            this.setState({
              showfooter: false
            });
          }
          ticking = false;
        });
     
        ticking = true;
      }
    };

    componentDidMount() {
      window.addEventListener('scroll', this.state.listener);
       store.dispatch({type:'CHAP_LOADING'});
        fetch('m/'+this.props.match.params.manga+'/'+this.props.match.params.chapter, {credentials: 'same-origin'})
        .then(response=>response.json())
        .then(json=>{
          store.dispatch({type:'CHAP_LOADED',chapter:json});
        })
    }

    componentWillReceiveProps(nextProps){
      if(nextProps.match.params.chapter && nextProps.match.params.chapter!=this.props.match.params.chapter){
        store.dispatch({type:'CHAP_LOADING'});
         fetch('m/'+nextProps.match.params.manga+'/'+nextProps.match.params.chapter, {credentials: 'same-origin'})
         .then(response=>response.json())
         .then(json=>{
           store.dispatch({type:'CHAP_LOADED',chapter:json});
         })
      }
    }

    render() {
      let nextchap = null,prevchap = null;
      
      if(this.state.showfooter){
        if (this.props.nextchap)
        nextchap = <Link to={'/m/'+this.props.match.params.manga+'/'+this.props.nextchap} className="navbutton"><i className="fa fa-arrow-right" aria-hidden="true"></i></Link>
        if (this.props.prevchap)
          prevchap = <Link to={'/m/'+this.props.match.params.manga+'/'+this.props.prevchap}  className="navbutton"><i className="fa fa-arrow-left" aria-hidden="true"></i></Link>

      }
      
      if(this.props.loading){
        return (<Loader/>)
      }else if(this.props.unavailable){
        return (
             <div className="chapter">
                  <div className="unavailable">
                  Chapter not yet released
                </div>
                <div className="navbuttons">
                {prevchap} {this.props.current} {nextchap}
                </div>
             </div>
        );
      }else{
        return (
             <div className="chapter">
                <div className={"navbuttons "+(this.state.showfooter?'':'hidden')}>
                {prevchap}<span className="current">{this.props.current}</span>{nextchap}
                </div>
                   {this.props.images.map(function(image) {
                    return (<div key={image}  className="row">
                    <Preload images={[image]} loadingIndicator={loadingIndicator}>
                        <img src={image} />
                    </Preload>
                    
                    </div>)
                   })}
             </div>
        );
      }
    }
}

const mapStateToProps = function(store) {
  return {
    showfooter : false,
    unavailable : store.currentChapter?store.currentChapter.unavailable:undefined,
    nextchap : store.currentChapter?store.currentChapter.nextchap:undefined,
    prevchap : store.currentChapter?store.currentChapter.prevchap:undefined,
    current : store.currentChapter?store.currentChapter.chapterid:undefined,
    images: store.currentChapter?store.currentChapter.images:[],
    loading: store.loading
  };
}

export default connect(mapStateToProps)(Chapter);
