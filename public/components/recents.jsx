import React from 'react';
import MangaListItem from './manga/list-item'
import Loader from './loader';
import store from '../store';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';

class Recents extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          recents : []
        };
      }

    componentDidMount() {
      store.dispatch({type:'RECENTS_LOADING'});
        fetch('/m/recents', {credentials: 'same-origin'})
        .then(response=>response.json())
        .then(json=>{
          store.dispatch({type:'RECENTS_LOADED'});
          this.setState({
            recents: json.recents || []
          });
        })
    }

    render() {
      if(this.props.loading){
        return (<Loader/>)
      }else{
        return (
              <ul>
                  {this.state.recents.map(row=>(<li key={row._id}>
                    <div className='list-item'>
                      <Link to={'/m/' + row.manga+'/'+row.chapter}>{row.name}</Link>
                    </div>
                    </li>)
                  )
                  }
              </ul>
        );
      }

    }
  }

  const mapStateToProps = function(store) {
    return {
      loading: store.loading
    };
  }

  export default connect(mapStateToProps)(Recents);
