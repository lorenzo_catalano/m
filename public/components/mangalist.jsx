import React from 'react';
import {Link} from 'react-router-dom';
import store from '../store';
import { connect } from 'react-redux';
import MangaListItem from './manga/list-item';
import Loader from './loader';

class MangaList extends React.Component {

    componentDidMount() {

      if(this.props.latest.length>0){
        store.dispatch({type:'MANGA_LATEST',resp:{rows:this.props.latest}});
      }else{
        store.dispatch({type:'HOME'});
        fetch('/m/latest',{credentials: 'same-origin'})
        .then(response=>response.json())
        .then(json=>{
          store.dispatch({type:'MANGA_LATEST',resp:json});
        })
      }
    }

    render() {
      //return (<Loader/>)
      if(this.props.loading){
        return (<Loader/>)
      }else{
        return (
              <div>
                  {this.props.latest.map(row=><div key={row.name} ><MangaListItem manga={row}/></div>)}
              </div>
        );
      }
    }
}
const mapStateToProps = function(store) {
  return {
    logged: store.logged,
    latest: store.latest,
    loading: store.loading
  };
}

export default connect(mapStateToProps)(MangaList);
