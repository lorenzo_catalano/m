import React from 'react';
import {Link} from 'react-router-dom';

class MangaListItem extends React.Component {

  unsubscribe(){
    fetch('/s', {
      credentials: 'same-origin',
      method: 'DELETE',
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
      body: JSON.stringify({
        path:this.props.manga.path,
        name:this.props.manga.name
      })
    }).then(()=>{
      this.setState(Object.assign({},this.state,{subscribed:false}));
    });
  }

  render() {
    var action = null;
    if(this.props.manga.subscribed){
      action=<i className="fa fa-bell" aria-hidden="true"></i>
    }else if(this.props.sub){
      action=<i className="fa fa-trash" aria-hidden="true" onClick={this.unsubscribe.bind(this)}></i>
    }
    return (
      <div className="list-item">
        <Link to={'/m/' + this.props.manga.path}>{this.props.manga.name}</Link>
        {action}
      </div>
    )
  }
}
export default MangaListItem;
