import React from 'react';


class AppContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          latest: []
        };
      }

    componentDidMount() {
        fetch("/latest", { credentials: 'same-origin' })
        .then(response=>response.json())
        .then(json=>{
          this.setState({
            latest: json.rows
          });
        })
    }

    render() {
        return (
              <ul>
                  {this.state.latest.map(function(row) {
                    return <li key={row.name}>{row.name}</li>
                  })}
              </ul>
        );
    }
};

export default AppContainer;
