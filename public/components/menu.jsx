import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

class Menu extends React.Component {

  closemenu() {
    document.querySelector('#menu').className = '';
  }

  render() {

    let logbutton = null,
        recents=null,
        subscribed=null;

    if (this.props.logged) {
      logbutton = <div className="actions-item list-item"><a href="/logout">Logout</a></div>
      recents = <div className="actions-item list-item">
                  <Link to='/recents' onClick={this.closemenu}>Recents</Link>
                </div>
      subscribed = <div className="actions-item list-item">
                    <Link to='/subscribed' onClick={this.closemenu}>Subscribed</Link>
                  </div>
    } else {
      logbutton = <div className="actions-item list-item"></div>
      recents = null
      subscribed = null
    }

    return (
      <aside id="menu">
        <div className="actions-item list-item">
          <Link to='/' onClick={this.closemenu}>Home</Link>
        </div>
        {subscribed}
        {recents}
        {logbutton}
      </aside>
    )
  }
}

const mapStateToProps = function(store) {
  return {logged: store.logged};
}

export default connect(mapStateToProps)(Menu);
