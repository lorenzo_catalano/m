import React from 'react';
import {Link} from 'react-router-dom';

class ChapterListItem extends React.Component {
  render() {
    return (
      <div className={this.props.chapter.read?'read list-item':'list-item'}>
        <Link to={`/m/${this.props.chapter.mangaPath}/${this.props.chapter.path}`}>{this.props.chapter.name}</Link>
      </div>
    )
  }
}
export default ChapterListItem;
