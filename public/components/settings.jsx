import React from 'react';
import store from '../store';
import { connect } from 'react-redux';
import Loader from './loader';

class Settings extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        devices : []
      };
    }

  componentDidMount() {
    store.dispatch({type:'SETTINGS_LOADING'});
      fetch('/settings', {credentials: 'same-origin'})
      .then(response=>response.json())
      .then(json=>{
        this.setState({
          devices: json.devices || []
        });
        store.dispatch({type:'SETTINGS_LOADED'});
      })
  }

  render() {
    if(this.props.loading){
      return (<Loader/>)
    }else{
      return (
          <div>
            <h4>Devices linked to this account</h4>
            <ul>
                {this.state.devices.filter(row=>row.active).map(row=><li key={row.iden} >{row.nickname}</li>)}
            </ul>
          </div>
      );
    }

  }
}
const mapStateToProps = function(store) {
  return {
    loading: store.loading
  };
}
export default connect(mapStateToProps)(Settings);
