import React from 'react';

class TelegramLoginButton extends React.Component {
  componentDidMount() {
    const { botName, buttonSize, cornerRadius, requestAccess, usePic, dataOnauth } = this.props;
    window.TelegramLoginWidget = {
      dataOnauth: user => dataOnauth(user)
    };

    <script async src="https://telegram.org/js/telegram-widget.js?4" data-telegram-login="lorenz_notification_bot" data-size="medium" data-auth-url="http://pi.lorenzocatalano.it/login" data-request-access="write"></script>

    const script = document.createElement('script');
    script.src = 'https://telegram.org/js/telegram-widget.js?4';
    script.setAttribute('data-telegram-login', botName);
    script.setAttribute('data-size', 'medium');
    script.setAttribute('data-request-access', requestAccess);
    script.setAttribute('data-userpic', true);
    script.setAttribute('data-auth-url', 'https://pi.lorenzocatalano.it/login');
    script.async = true;
    this.instance.appendChild(script);
  }


  render() {
    return (
      <div
        className={this.props.className}
        ref={component => {
          this.instance = component;
        }}
      >
        {this.props.children}
      </div>
    );
  }
}

export default TelegramLoginButton;