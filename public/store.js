import { createStore } from 'redux';

const initialState = {
			title:'Latest',
			logged:window.logged,
			latest:[],
			loading:true,
			currentChapter:undefined,
			currentChapterName:undefined,
			currentMangaPath:undefined,
			currentMangaName:undefined,
			profile:{}
		};

var reducer = function(state, action) {
	if (state === undefined) {
    return initialState;
  }else if (action.type === 'HOME') {
		return Object.assign({},state,{
			currentChapter:undefined,
			currentChapterName:undefined,
			currentMangaPath:undefined,
			currentMangaName:undefined,
			title:'Latest',
			loading:true
		});
	}else if (action.type === 'SET_TITLE') {
    return Object.assign({},state,{title:action.title});
  }else if(action.type==='MANGA_LATEST'){
		return Object.assign({},state,{
			currentChapter:undefined,
			currentChapterName:undefined,
			currentMangaPath:undefined,
			currentMangaName:undefined,
			latest:action.resp.rows,
			title:'Latest',
			loading:false
		});
	}else if(action.type.endsWith('_LOADING')){
		return Object.assign({},state,{
			loading:true
		});
	}else if(action.type==='MANGA_LOADED'){
		return Object.assign({},state,{
			subscribed:action.manga.subscribed,
			currentChapter:undefined,
			currentChapterName:undefined,
			currentMangaPath:action.manga.path,
			currentMangaName:action.manga.name,
			currentManga:action.manga,
			title:action.manga.name,
			loading:false
		});
	}else if(action.type==='CHAP_LOADED'){
		return Object.assign({},state,{
			subscribed:action.chapter.subscribed,
			//currentMangaPath:action.chapter.mangaPath,
			currentChapter:action.chapter,
			currentChapterName:action.chapter.name,
			title:action.chapter.name,
			loading:false,
			unavailable:action.chapter.unavailable
		});
	}else if(action.type==='SUBS_LOADED'){
		return Object.assign({},state,{
			currentChapter:undefined,
			currentChapterName:undefined,
			currentMangaPath:undefined,
			currentMangaName:undefined,
			subscriptions:action.subscriptions,
			title:'Subscriptions',
			loading:false
		});
	}else if(action.type==='SETTINGS_LOADED'){
		return Object.assign({},state,{
			currentChapter:undefined,
			currentChapterName:undefined,
			currentMangaPath:undefined,
			currentMangaName:undefined,
			title:'Settings',
			loading:false
		});
	}else if(action.type==='RECENTS_LOADED'){
		return Object.assign({},state,{
			currentChapter:undefined,
			currentChapterName:undefined,
			currentMangaPath:undefined,
			currentMangaName:undefined,
			title:'Recents',
			loading:false
		});
	}else if(action.type==='SUBSCRIBE'){
		return Object.assign({},state,{
			subscribed:true
		});
	}else if(action.type==='UNSUBSCRIBE'){
		return Object.assign({},state,{
			subscribed:false
		});
	}


  return state;
}

const store = createStore(reducer);
export default store;
