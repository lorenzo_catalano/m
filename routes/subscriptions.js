const {Manga} = require('../classes');

module.exports = function(db) {
  var express = require('express');
  var router = express.Router();
  router.get('/', function(req, res) {
    if (req.session && req.session.user) {
      db.find({
        type: 'subscription',
        iden: req.session.user.iden
      }, function(err, subscriptions) {

        var subs = subscriptions.map(s=>{
            return new Manga(s.name,s.path)
        }) 

        res.send({success: true, subscriptions: subs});
        return;
      })
    } else {
      res.send({success: false, error: 'Unauthorized'});
    }

  })

  router.post('/', function(req, res) {
    var path = req.body.path;
    var name = req.body.name;
    var subscription = {
      type: 'subscription',
      path: path,
      iden: req.session.user.iden,
      name: name,
      active: true
    }
    db.update({
      type: 'subscription',
      path: path,
      iden: req.session.user.iden,
      name: name
    }, subscription, {
      upsert: true
    }, function(err, newDoc) {

      if (err) {
        res.send({success: false})
      } else {
        res.send({success: true});
      }
      return;
    });

  });

  router.delete('/', function(req, res) {
    var path = req.body.path;
    db.remove({
      type: 'subscription',
      path: path,
      iden: req.session.user.iden
    }, function(err) {
      if (err) {
        res.send({success: false})
      } else {
        res.send({success: true});
      }
    });
  });

  router.post('/on', function(req, res) {
    var id = req.body.id;
    db.update({
      type: 'subscription',
      _id: id,
      iden: req.session.user.iden
    }, {
      $set: { active: true }
    },{
      returnUpdatedDocs :true
    }, function(err, subscription) { // Callback is optional
      if (err) {
        res.send({success: false})
      } else {
        res.send({success: true,subscription:subscription});
      }
    });
  });
  router.post('/off', function(req, res) {
    var id = req.body.id;
    db.update({
      type: 'subscription',
      _id: id,
      iden: req.session.user.iden
    }, {
      $set: { active: false }
    },{
      returnUpdatedDocs :true
    }, function(err, subscription) { // Callback is optional
      if (err) {
        res.send({success: false})
      } else {
        res.send({success: true,subscription:subscription});
      }
    });
  });

  return router;
}
