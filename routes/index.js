module.exports = function(db){
  var express = require('express');
  var request = require('request-promise');
  var debug = require('debug')('m');

  var router = express.Router();

  router.get('/',function(req,res){
    var logged = !!(req.session && req.session.user && req.session.user.iden)
    debug('user logged:'+logged)
    res.render('index',{
          title:'Latest Manga',
          logged:logged
    })
  })


  router.get('/logindev',function(req,res){
    var user = {
        type:'user',
        name:'dev',
        iden:'dev',
        image_url:'dev',
        active:true,
        token:'dev'
    }
    req.session.user = user;
    req.session.save();
    res.redirect('/');
  })

  router.get('/login',function(req,res){
      debug(req.query)
      /*{ id: '14524676',
         first_name : 'Lorenzo',
         last_name: 'Catalano',
         username: 'Mapiasa',
         photo_url: 'https://t.me/i/userpic/320/Mapiasa.jpg',
         auth_date: '1538602969',
         hash: 'e01d753ba0c0360575eb1aa748942de27575820198dc0df8a5be98f64f583ac6' }*/

      var user = {
        $set: { 
        'type':'user',
        'iden': req.query.id,
        'active': true,
        'name': req.query.username,
        'photo_url':req.query.photo_url
        }
      }

      db.updateP(
        {iden:req.query.id},
        user,
        {returnUpdatedDocs:true,upsert:true}
      ).then(updateduser=>{
        req.session.user = updateduser;
        req.session.save();
        res.redirect('/');
      })

  })

  router.get('/logout',function(req,res){
    req.session.destroy(function(err) {
  res.redirect('/')
});

  })

  router.get('/users', function (req, res) {
          db.find({ type:'user'}, function (err, users) {
              if(err){
                  res.send({success:false})
              }else{
                  res.send({
                      success:true,
                      users:users
                  });
              }
          });
  });
  return router;
}
