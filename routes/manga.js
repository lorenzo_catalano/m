const {Manga,Chapter} = require('../classes');
var debug = require('debug')('m');
module.exports = function(db) {
  var express = require('express');
  var request = require('request-promise').defaults({
    //proxy:'http://conscatalanolorenzo:CLPassw1@10.45.5.50:8080',
    //strictSSL :false
  });
  var router = express.Router();
  var url_host = 'http://www.mangapanda.com';
  var url_latest = 'http://www.mangapanda.com/latest';

  router.get('/latest', function(req, res) {

    var regexlatest = /<a class="chapter" href="\/(.*)"><strong>(.*)<\/strong><\/a>[\s\S]*?<a class="chaptersrec" href="\/(.*)">(.*)<\/a>/g
    var ps = [];
    ps.push(request(url_latest))
    if (req.session.user) {
      ps.push(db.findP({type: 'subscription', iden: req.session.user.iden}))
    }

    Promise.all(ps).then(function(results) {
      const [html,subscriptions] = results;
      if (subscriptions) {
        const subsmangalink = subscriptions.map(s => s.path)
        req.session.user.subscriptions = subsmangalink;
        req.session.save();
      }

      var match = regexlatest.exec(html)
      const latest = []
      while(match){
          var manga = new Manga(match[2],match[1])
          manga.last = match[3]
          manga.lastname = match[4]
          manga.subscribed = req.session.user && !!req.session.user.subscriptions.find(x => x == manga.path)
          latest.push(manga)
          match = regexlatest.exec(html)
      }
      res.send({
        success: true,
        rows: latest || []
      })
    }).catch(function(err) {
      debug(err)
      res.send({success: false})
    })

  });

  router.get('/recents', function(req, res) {
    db.find({type: 'recents'}).sort({date: -1}).limit(100).exec(function(err, recents) {
      if (err) {
        res.send({success: false})
      } else {
        res.send({success: true, recents: recents});
      }
    });
  })

  router.get('/:mangaid', function(req, res) {
    var chapters = [];
    var promises = []
    promises.push(request(url_host +'/'+req.params.mangaid));

    if (req.session && req.session.user) {
      promises.push(db.findP({
          type: 'recents',
          manga: req.params.mangaid,
          iden: req.session.user.iden
        })
      )
    }

    Promise.all(promises).then(function(results) {
      const html = results[0];
      const recentss = results[1] && results[1].map(x => x.chapter) || [];
      var name = html.match(/<h2 class="aname">(.*)<\/h2>/)[1]
      var regexchapters = new RegExp('<a href="/'+req.params.mangaid+'/(.*)">(.*)</a>.*</td>[\\s\\S]*?<td>(\\d{2})/(\\d{2})/(\\d{4})</td>','g')

      var match = regexchapters.exec(html)
      var chapters = []
      while(match){
        var chapter = new Chapter(match[2],match[1])
        chapter.read = recentss.indexOf(chapter.path) >= 0
        chapter.mangaPath = req.params.mangaid
          chapters.push(chapter)
          match = regexchapters.exec(html)
      }

      const resp = {
        path: req.params.mangaid,
        name: name,
        success: true,
        chapters: chapters.reverse()
      }
      if(req.session.user && req.session.user.subscriptions){
        resp.subscribed= !!req.session.user.subscriptions.find(x => x == '/'+req.params.mangaid)
      }
      res.send(resp)
    });
  })

  router.get('/:mangaid/:chapter', function(req, res) {
    const manga = req.params.mangaid
    const chapter = req.params.chapter
    const model = {
      success: true,
      unavailable: false,
      chapterid: + chapter,
      prevchap: undefined,
      nextchap: undefined,
      mangaid: manga,
      images: []
    };



    var nextchappreload
    request(url_host + '/' + manga + '/' + chapter).then(function(html) {
      if (html.indexOf('not released yet') > 0) {
        model.name = html.match(/<h1>(.*)<\/h1>/)[1]
        model.unavailable = true;
        model.prevchap = (+ chapter - 1);
      } else {

        var pagesselect = html.match(/(<select id="pageMenu" [\s\S]*<\/select>)/gm)[0]
        var opreg = /(<option value="([\s\S]*?)"[\s\S]*?>[\s\S]*?<\/option>)/gm
        model.name = html.match(/<h1>(.*)<\/h1>/)[1]
        //segno il capitolo come gia letto
        if (req.session && req.session.user) {
          let recents = {
            type: 'recents',
            iden: req.session.user.iden,
            name: model.name,
            manga: manga,
            chapter: chapter,
            date: new Date()
          }

          db.updateP({
            type: 'recents',
            iden: req.session.user.iden,
            manga: manga,
            chapter: chapter
          },recents,{returnUpdatedDocs:true,upsert:true})
    
        }

        if (+ (chapter) > 1) {
          model.prevchap = (+ chapter - 1);
        }

        var regexnextchap = /chapternav_next[\s\S]*?<a href="(.*)">/
        var nextchap  = html.match(regexnextchap)[1]
        if(nextchap  && nextchap[1]){
          nextchappreload = nextchap
          model.nextchap = (+ chapter + 1);
        }
        var imagesPromises = [];
        var mmm
        while(mmm = opreg.exec(pagesselect)){
          imagesPromises.push(request('http://www.mangapanda.com' + mmm[2]))
        }
        return Promise.all(imagesPromises)
      }
    }).then(function(results) {
      if (results) {
        for (var x = 0; x < results.length; x++) {
          var regex = /<img id="img".*src="(.*?)"/
          var matches = results[x].match(regex)
          model.images.push(matches[1]);
        }
      }
      if(req.session.user && req.session.user.subscriptions){
        model.subscribed= !!req.session.user.subscriptions.find(x => x == manga)
      }
      res.send(model)

      //preload next chapter
      preload(...(nextchappreload.substring(1).split('/')))

    }).catch(function(err) {
      debug(err)
      res.send({success: false, error: err})
    });
  });

  function preload(manga,chapter){
    debug('preloading ',manga,chapter)
    const model = {
      success: true,
      unavailable: false,
      chapter: chapter,
      chapterid: + chapter,
      prevchap: undefined,
      nextchap: undefined,
      mangaid: manga,
      images: []
    };
    var nextchappreload
    request(url_host + '/' + manga + '/' + chapter).then(function(html) {
      if (html.indexOf('not released yet') > 0) {
        model.name = html.match(/<h1>(.*)<\/h1>/)[1]
        model.unavailable = true;
        model.prevchap = (+ chapter - 1);
      } else {

        var pagesselect = html.match(/(<select id="pageMenu" [\s\S]*<\/select>)/gm)[0]
        var opreg = /(<option value="([\s\S]*?)"[\s\S]*?>[\s\S]*?<\/option>)/gm
        model.name = html.match(/<h1>(.*)<\/h1>/)[1]

        if (+ (chapter) > 1) {
          model.prevchap = (+ chapter - 1);
        }

        var regexnextchap = /chapternav_next[\s\S]*?<a href="(.*)">/
        var nextchap  = html.match(regexnextchap)[1]
        nextchappreload = nextchap
        model.nextchap = (+ chapter + 1);
        var imagesPromises = [];
        var mmm
        while(mmm = opreg.exec(pagesselect)){
          imagesPromises.push(request('http://www.mangapanda.com' + mmm[2]))
        }
        return Promise.all(imagesPromises)
      }
    }).then(function(results) {
      if (results) {
        for (var x = 0; x < results.length; x++) {
          var regex = /<img id="img".*src="(.*?)"/
          var matches = results[x].match(regex)
          model.images.push(matches[1]);
        }
      }
      //if(req.session.user && req.session.user.subscriptions){
      //  model.subscribed= !!req.session.user.subscriptions.find(x => x == manga)
      //}
      debug('preload done ',manga,chapter)
    }).catch(function(err) {
      debug(err)
    });
  }

  return router;
}
