
 class Manga {
    constructor(name, path) {
        this.name = name;
        this.path = path;
        this.last = undefined
        this.lastname = undefined
        this.subscribed = undefined
    }
}

class Chapter{
    constructor(name, path) {
        this.name = name;
        this.path = path;
        this.month = undefined
        this.day = undefined
        this.year = undefined
        this.read = false
        this.mangaPath = undefined
    }
    fullPath(){
        return `${this.mangaPath}/${this.path}`
    }
}
module.exports = {
    Manga:  Manga,
    Chapter:Chapter
}