var Datastore = require('nedb');
var dbpath = __dirname+'/nedb';
var db = new Datastore({filename: dbpath, autoload: true})

db.findP = function(query){
    return new Promise(function(resolve, reject) {
      db.find(query, function(err, res) {
        if (err) {
          reject(err)
        } else {
          resolve(res)
        }
      })
    })
  }

  db.updateP = function(query, update, options){
    return new Promise(function(resolve, reject) {
      db.update(query, update, options,function(err, numAffected, affectedDocuments, upsert){
        if (err) {
          reject(err)
        } else {
          resolve(affectedDocuments)
        }
      })
    })
  }

  var user = {
    $set: { 
      'type':'user',
      'token': '',
      'active': true,
      'name': 'my name 2'
     }
  }
  
  db.updateP(
      {'type':'user','iden':'pippo'},
      user,
      {returnUpdatedDocs:true,upsert:true}
  ).then(jp=>{
    console.log(JSON.stringify(jp))
    return true
})