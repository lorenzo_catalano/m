var HOME = require('os').homedir()
var debug = require('debug')('m');



var express = require('express');
var compression = require('compression');
var session = require('express-session');
var path = require('path');

var request = require('request-promise')

var CronJob = require('cron').CronJob;

//var favicon = require('serve-favicon');
debug.log = console.log.bind(console);

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var NedbStore = require('nedb-session-store')(session);

var Datastore = require('nedb');
var dbpath = __dirname+'/nedb';
var db = new Datastore({filename: dbpath, autoload: true})

const botToken = 'bot465714259:AAFw8X8m_-D3k-r7cA9dGU2iaHtrgFg6r0U'
const telegramEndpoint = `https://api.telegram.org/${botToken}/sendMessage`

db.findP = function(query){
  return new Promise(function(resolve, reject) {
    db.find(query, function(err, res) {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}
db.findOneP = function(query){
  return new Promise(function(resolve, reject) {
    db.findOne(query, function(err, res) {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

db.insertP = function(item){
  return new Promise(function(resolve, reject) {
    db.insert(item, function(err, res) {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}
db.updateP = function(query, update, options){
  return new Promise(function(resolve, reject) {
    db.update(query, update, options,function(err, numAffected, affectedDocuments, upsert){
      if (err) {
        reject(err)
      } else {
        resolve(affectedDocuments)
      }
    })
  })
}




var index = require('./routes/index')(db);
var manga = require('./routes/manga')(db);
var subscriptions = require('./routes/subscriptions')(db);

//var index = require('./routes/index');
//var admin = require('./routes/admin');

var app = express();

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use(compression());
app.use(session({
  resave: false,
  saveUninitialized: false,
  secret: 'yoursecret',
  key: 'sid',
  cookie: {
    path: '/',
    httpOnly: true,
    maxAge: 365 * 24 * 3600 * 1000 // One year for example
  },
  store: new NedbStore({
    filename: __dirname+'/nedbsessions' 
  })
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(cookieParser());

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(function(req,res,next){
  debug(`${req.method} ${req.path}`)
  next()
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(__dirname, 'dist')));

app.use('/', index);
app.use('/m', manga);
app.use('/s', subscriptions);
app.get('/test',(req,res) => {manageSubs();res.send('ok')})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err, req, res);
});
// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    debug(err);
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

let notified = {}

const resetNotified = new CronJob({
  cronTime: '0 0 0 * * *',
  onTick: function(){
    notified = {}
  }
});
resetNotified.start();

let manageSubs = function(){
  let now = new Date()
  let day = (now.getDate()+'').padStart(2,'0')
  let month = ((now.getMonth()+1)+'').padStart(2,'0')
  let year = now.getFullYear()
  let today = `${month}\/${day}\/${year}`
  debug(`start notification process:${today}`)
  //
   db.find({
     type: 'subscription',
     active:true,
   }, function(err, subs) {
     if(err){
       debug(err)
       return;
     }
     var regs = `<a href="(.*)">(.*)<\/a>.*<\/td>\\s.*<td>${month}\/${day}\/${now.getFullYear()}<\/td>`
     let regex = new RegExp(regs,'gm')
     subs.forEach(s => {
       debug(`start notify ${s.name} to ${s.iden}`)
       request('http://www.mangapanda.com/'+s.path).then(html=> {
         let matches = regex.exec(html);
         while(matches){
           let link = matches[1]
           let name = matches[2]
           debug(`matched ${link} match ${name}`)
           let useriden = s.iden
           if(!notified[link+'_'+useriden]){
            notified[link+'_'+useriden] = true
            db.findOneP({
              type: 'user',
              iden:useriden
            }).then(u=>{
              var text=`*${name}*\n[Leggi](${process.env.APP_URL}/#/m${link})`

              debug(text)
              request.post(telegramEndpoint, {
                form: {
                  parse_mode: 'Markdown',
                  chat_id: useriden,
                  text: text
                }
              },function (error) {
                if(error)debug('error:', error); 
              })

            }).catch(err=>{
              debug(err)
            })
             }
           matches = regex.exec(html);
         }
       }).catch(function(err){
         debug(`requesting ${s.url} error`)
         debug(err)
       })
    })
   })
}


 const notifyJob = new CronJob({
   cronTime: '0 */5 * * * *',
   onTick: manageSubs
  });
 notifyJob.start();


module.exports = app;
