'use strict';

var debug = require('debug')('m');
debug.log = console.log.bind(console);
require('./server.js');

process.on('SIGINT', function() {
  debug( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
  // some other closing procedures go here
  process.exit(1);
});