'use strict';

var debug = require('debug')('m');
var app = require('./app');

var fs = require('fs')
var http = require('http')

//var privateKey  = fs.readFileSync(__dirname+'/certs/server.key', 'utf8');
//var certificate = fs.readFileSync(__dirname+'/certs/server.cert', 'utf8');

/*
const privateKey = fs.readFileSync('/etc/letsencrypt/live/pi.lorenzocatalano.it/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/pi.lorenzocatalano.it/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/pi.lorenzocatalano.it/chain.pem', 'utf8');
*/
const credentials = {
	///key: privateKey,
	//cert: certificate,
	//ca: ca
};



debug.log = console.log.bind(console);

process.env.APP_URL = `https://pi.lorenzocatalano.it`
app.set('port', process.env.PORT);

debug(process.env)

http.createServer(app)
.listen(process.env.PORT, function () {
  debug('Express server listening on port ' + process.env.PORT);
})
